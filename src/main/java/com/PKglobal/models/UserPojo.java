package com.PKglobal.models;

public class UserPojo {

	/*
	 * { "first_name": "harish", "last_name" : "meda", "gender": "male", "dob":
	 * "1985-08-12", "email": "mh@yopmail.com", "phone": "9765344556", "website":
	 * "https://hhhhfm.com", "address": "19,perungudi,chennai", "status": "active" }
	 */

	private String first_name;

	private String last_name;

	private String gender;
	
	private String DOB;



	private String email;

	private String phone;

	private String website;

	private String address;

	private String status;

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getDOB() {
		return DOB;
	}

	public void setDOB(String dOB) {
		DOB = dOB;
	}

}
