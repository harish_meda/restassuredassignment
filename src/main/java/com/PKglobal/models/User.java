package com.PKglobal.models;

public class User {

	public User() {

	}
	
	
	public User(String first_name, String last_name, String gender, String dOB, String email, double phone,
			String website, String address, String status) {
	
		this.first_name = first_name;
		this.last_name = last_name;
		this.gender = gender;
		this.DOB = dOB;
		this.email = email;
		this.phone = phone;
		this.website = website;
		this.address = address;
		this.status = status;
	}
	
	
/*	public User(String first_name,String last_name,String gender,String DOB,String email,String phone,String website, String address,String status) {
		
		
		
	}*/

	private String first_name;

	private String last_name;

	private String gender;

	private String DOB;

	private String email;

	private double phone;

	private String website;

	private String address;

	private String status;

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getDOB() {
		return DOB;
	}

	public void setDOB(String d) {
		DOB = d;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public double getPhone() {
		return phone;
	}

	public void setPhone(double d) {
		this.phone = d;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}


	@Override
	public String toString() {
		return "User [first_name=" + first_name + ", last_name=" + last_name + ", gender=" + gender + ", DOB=" + DOB
				+ ", email=" + email + ", phone=" + phone + ", website=" + website + ", address=" + address
				+ ", status=" + status + "]";
	}
	
	

}
