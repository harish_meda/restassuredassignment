package com.PKglobal.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.PKglobal.models.User;
import com.PKglobal.models.UserPojo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Testclass {
	
	static String excelFilePath = System.getProperty("user.dir")+"\\TestData\\UsersTestdata.xlsx";
	
	public static void main(String[] args) throws FileNotFoundException, IOException, ParseException {
		
		
		
		
		  // Step 1: Read Excel File into Java List Objects
	    List<User> users = readExcelFile();
	    
	    // Step 2: Convert Java Objects to JSON String
	    String jsonString = convertObjects2JsonString(users);
	    
	    System.out.println(jsonString);
		
		
		
	

}
	
	
	
	
	public  static List<User> readExcelFile() {
	    try {
	      FileInputStream excelFile = new FileInputStream(new File(excelFilePath));
	        Workbook workbook = new XSSFWorkbook(excelFile);
	     
	        Sheet sheet = workbook.getSheet("Users");
	        Iterator<Row> rows = sheet.iterator();
	        
	        List<User> userList = new ArrayList<User>();
	        
	        int rowNumber = 0;
	        while (rows.hasNext()) {
	          Row currentRow = rows.next();
	          
	          // skip header
	          if(rowNumber == 0) {
	            rowNumber++;
	            continue;
	          }
	          
	          Iterator<Cell> cellsInRow = currentRow.iterator();
	 
	          User user = new User();
	          
	          int cellIndex = 0;
	          while (cellsInRow.hasNext()) {
	            Cell currentCell = cellsInRow.next();
	            
	             if(cellIndex==0) { 
	            	user.setFirst_name(currentCell.getStringCellValue());
	            	
	            	
	            } else if(cellIndex==1) { 
	            	user.setLast_name(currentCell.getStringCellValue());
	            }
	             else if(cellIndex==2) { 
	            	user.setGender(currentCell.getStringCellValue());
	            } else if(cellIndex==3) { 
	            	user.setDOB(currentCell.getStringCellValue());
	            }
	            
	            else if(cellIndex==4) {
	            	user.setEmail(currentCell.getStringCellValue());
	            }
	            
	            else if(cellIndex==5) { 
	            	user.setPhone(currentCell.getNumericCellValue());
	            }
	            
	            else if(cellIndex==6) { 
	            	user.setWebsite(currentCell.getStringCellValue());
	            }
	            
	            else if(cellIndex==7) { 
	            	user.setAddress(currentCell.getStringCellValue());
	            }
	            else if(cellIndex==8) { 
	            	user.setStatus(currentCell.getStringCellValue());
	            }
	            
	            cellIndex++;
	          }
	          
	          userList.add(user);
	        }
	        
	        // Close WorkBook
	        workbook.close();
	        
	        return userList;
	        } 
	          catch (IOException e) {
	          throw new RuntimeException("FAIL! -> message = " + e.getMessage());
	        }
	  }
	
	
	
	private static String convertObjects2JsonString(List<User> users) {
	      ObjectMapper mapper = new ObjectMapper();
	      String jsonString = "";
	      
	      try {
	        jsonString = mapper.writeValueAsString(users);
	      } catch (JsonProcessingException e) {
	        e.printStackTrace();
	      }
	      
	      return jsonString; 
	  }
	
	
	
	
}