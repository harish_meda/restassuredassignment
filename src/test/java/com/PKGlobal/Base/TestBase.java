package com.PKGlobal.Base;

import java.io.IOException;

import org.testng.annotations.BeforeTest;

import com.PKglobal.utils.Utils;

import io.restassured.RestAssured;

public class TestBase extends Utils {

	String token = "FhH_DOb_ETdwrmbk6V75jNdigXVskujKRS23";

	@BeforeTest
	public void init() {

		RestAssured.baseURI = "https://gorest.co.in/";

		RestAssured.basePath = "public-api";

		RestAssured.authentication = RestAssured.oauth2(token);

	}

	@org.testng.annotations.DataProvider(name = "UserTestData")
	@BeforeTest
	public String[][] getData() throws IOException {
		String excelFilePath = System.getProperty("user.dir") + "\\TestData\\UsersTestData.xlsx";
		int rownum = ExcelUtils.getRowCount(excelFilePath, "Users");
		int colcount = ExcelUtils.getCellCount(excelFilePath, "Users", 1);

		String xldata[][] = new String[rownum][colcount];
		for (int i = 1; i <= rownum; i++) {
			for (int j = 0; j < colcount; j++) {
				xldata[i - 1][j] = ExcelUtils.getCellData(excelFilePath, "Users", i, j);
			}
		}
		return (xldata);
	}

}
