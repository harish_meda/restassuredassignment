package com.PKGlobal.TestCases;

import static io.restassured.RestAssured.given;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.PKGlobal.Base.TestBase;
import com.PKglobal.models.UserPojo;
import com.jayway.jsonpath.JsonPath;

import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class APITestCases extends TestBase {

	public static Response response;
	public static String jsonAsString;
	static String jsonResonse;
	String userID;

	@Test
	public void createNewUserWithPOJO() throws ParseException {

		UserPojo user = new UserPojo();
		user.setFirst_name("Hhh");
		user.setLast_name("kjk");
		user.setGender("male");
		user.setDOB("12-08-89");
		user.setEmail("huhu.m@gmail.com");
		user.setPhone("2389556908");
		user.setWebsite("https://hhhhfm.com");
		user.setAddress("22,kk,near kio,chennai");
		user.setStatus("active");

		Response response = given().contentType(ContentType.JSON)

				.body(user).when().post("/users").then().using().extract().response();

		String body = response.getBody().asString();

		JSONParser parser = new JSONParser();

		JSONObject jsonBody = (JSONObject) parser.parse(body);

		jsonWriter(jsonBody);

		int Status = response.getStatusCode();
		System.out.println(body);
		Assert.assertEquals(Status, 200);
	}

	@Test(dependsOnMethods = "getUserTest")
	public void deletePojoUser() throws FileNotFoundException, IOException, ParseException {

		
		userID = (String) jsonReader().get("id");

		Response response = given().contentType(ContentType.JSON).when().delete("users/" + userID)

				.then().using().extract().response();

		
		int Status = response.getStatusCode();
		System.out.println(Status);
		Assert.assertEquals(Status, 200);

	}

	@Test(dependsOnMethods = "createNewUserWithPOJO")
	public void getUserTest() throws FileNotFoundException, IOException, ParseException {

		userID = (String) jsonReader().get("id");
		String firstname = (String) jsonReader().get("first_name");

		System.out.println("sdsd" + userID);

		jsonResonse = given().when().get("users/" + userID).asString();

		String id = JsonPath.read(jsonResonse, "$.result.id");
		String name = JsonPath.read(jsonResonse, "$.result.first_name");

		Assert.assertEquals(name, firstname);
		Assert.assertEquals(id, userID);
	}

	@Test(dataProvider = "UserTestData")
	public void createNewUserWithDD(String first_name, String last_name, String gender, String dob, String email,
			String phone, String website, String address, String status) throws ParseException {

		Map<String, String> pathParams = new HashMap<String, String>();
		pathParams.put("first_name", first_name);
		pathParams.put("last_name", last_name);
		pathParams.put("gender", gender);
		pathParams.put("dob", dob);
		pathParams.put("email", email);
		pathParams.put("phone", phone);
		pathParams.put("website", website);
		pathParams.put("address", address);
		pathParams.put("status", status);

		Response response = given().contentType(ContentType.JSON)

				.body(pathParams).when().post("/users").then().using().extract().response();

		String body = response.getBody().asString();

		JSONParser parser = new JSONParser();

		JSONObject jsonBody = (JSONObject) parser.parse(body);

		jsonWriter(jsonBody);

		int Status = response.getStatusCode();

		Assert.assertEquals(Status, 200);

	}

	@Test(dependsOnMethods = "createNewUserWithDD")
	public void deleteDDUser() throws FileNotFoundException, IOException, ParseException {

		userID = (String) jsonReader().get("id");

		Response response = given().contentType(ContentType.JSON).when().delete("users/" + userID)

				.then().using().extract().response();

		int Status = response.getStatusCode();
		Assert.assertEquals(Status, 200);

	}

}